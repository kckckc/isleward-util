import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react';
import Input from './Input';
import { InputProps } from './Input.types';

export default {
	title: 'Input',
	component: Input
} as Meta;

const Template: Story<InputProps> = (args) => {
	const [value, setValue] = useState('');
	return <Input {...args} value={value} setValue={setValue} />
};

export const Default: Story<InputProps> = Template.bind({});
Default.args = {
	placeholder: 'type here'
}

export const WithError: Story<InputProps> = Template.bind({});
WithError.args = {
	placeholder: 'type here',
	indicator: '(!)',
	indicatorColor: 'yellow',
}

export const Layout: Story = () => <div>
	<p>asdasdasdasdasdasd</p>
	<div style={{
		display: 'flex',
		flexDirection: 'row',
		columnGap: '8px',
		width: '100%',
	}}>
		<div style={{
			flexGrow: 1
		}}>
			<Default placeholder='aaaaaa' />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default placeholder='bbbbbb' indicator="(!)" indicatorColor="red" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default placeholder='cccccc' indicator="(?)" indicatorColor="yellow" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default placeholder='dddddd' indicator="(i)" indicatorColor="blue" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default placeholder='eeeeee' indicator="✔" indicatorColor="green" />
		</div>
	</div>
	<p>asdasdasdasdasdasd</p>
</div>
