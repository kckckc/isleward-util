import React from 'react';
import classNames from 'classnames';

import { ButtonProps } from './Button.types';

import styles from './Button.module.scss';

const Button: React.FC<ButtonProps> = ({
	children = 'Button',
	onClick,
	className,
	negative = false,
	disabled = false,
}) => (
	<button
		className={classNames(
			className,
			styles.Button,
			negative && styles.negative,
			disabled && styles.disabled
		)}
		disabled={disabled}
		type="button"
		onClick={!disabled ? onClick : null}
	>
		{children}
	</button>
);

export default Button;
