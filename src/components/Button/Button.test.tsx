import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { composeStories } from '@storybook/testing-react';

import * as stories from './Button.stories';

const { Default } = composeStories(stories);

describe('Button', () => {
	
	it('renders with text', () => {
		render(<Default>some text content</Default>);

		const button = screen.getByRole('button');
		
		expect(button).toHaveTextContent('some text content');
	});

	it('calls "onClick" when clicked', () => {
		const onClick = jest.fn();

		render(<Default onClick={onClick} />);

		const btn = screen.getByRole('button');
		fireEvent.click(btn);
		expect(onClick).toHaveBeenCalled();
	});

});
