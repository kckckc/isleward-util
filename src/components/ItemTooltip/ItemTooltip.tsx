import React, {
	createContext,
	CSSProperties,
	useContext,
	useEffect,
	useLayoutEffect,
	useRef,
	useState,
} from 'react';
import merge from 'deepmerge';

import { ItemTooltipProps, ItemTooltipContextProps } from './ItemTooltip.types';

import styles from './ItemTooltip.module.scss';
import translateStatName from '../../calc/translateStatName';
import translateStatValue from '../../calc/translateStatValue';
import classNames from 'classnames';

// TODO: slot with tooltip, respect edges of screen
// https://gitlab.com/Isleward/isleward/-/blob/master/src/client/ui/templates/tooltipItem/tooltipItem.js#L44

// TODO: this stuff is soooooooooooooooooooooooooooooo messy

// TODO: implement worth
// TODO: implement compare

// Provider for item/compare data
const ItemTooltipContext = createContext<ItemTooltipContextProps>({
	item: null,
	compareItem: null,
	doCompare: false,
});

type Validator = (ctx: ItemTooltipContextProps) => boolean;

/// Generators

const GDiv: React.FC<{
	className: string;
	validate?: Validator;
	children?: React.ReactNode;
}> = ({ className, validate, children }) => {
	const ctx = useContext(ItemTooltipContext);

	// We duplicated a bunch of logic to write validators that will
	// only add the div if the child will render something
	if (validate && !validate(ctx)) {
		return null;
	}

	return <div className={className}>{children}</div>;
};

const VName: Validator = () => true;
const GName: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	let itemName = item.name;
	if (item.quantity > 1) {
		itemName += ` x${item.quantity}`;
	}

	return <>{itemName}</>;
};

const VType: Validator = ({ item }) => item.type && item.type !== item.name;
const GType: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.type || item.type === item.name) {
		return null;
	}

	return <>{item.type}</>;
};

const VPower: Validator = ({ item }) => !!item.power;
const GPower: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.power) {
		return null;
	}

	return <>{new Array(item.power + 1).join('+')}</>;
};

const VImplicitStats: Validator = ({ item }) => !!item.implicitStats;
// TODO refactor implicitstat/stat generators to reuse some code
const GImplicitStats: React.FC = () => {
	const { item, compareItem, doCompare } = useContext(ItemTooltipContext);

	// TODO check if this interferes with compareitem having implicits that we lose?
	if (!item.implicitStats) {
		return null;
	}

	const tempImplicitStats = merge([], item.implicitStats);
	let outTempImplictStats: { stat: string; value: string }[] = [];

	tempImplicitStats.forEach(({ stat, value }) => {
		outTempImplictStats.push({ stat, value: value.toString() });
	});

	// TODO check this condition?
	if (compareItem && doCompare /* && !item.eq */) {
		const compareImplicitStats = compareItem.implicitStats || [];

		outTempImplictStats = tempImplicitStats.map((s) => {
			const { stat, value } = s;
			const o = {
				stat,
				value: value.toString(),
			};

			const f = compareImplicitStats.find((c) => c.stat === stat);

			if (!f) {
				o.value = `+${value}`;
				return;
			}

			const delta = value - f.value;
			if (delta > 0) {
				o.value = `+${value}`;
			} else if (delta < 0) {
				o.value = delta.toString();
			}

			return o;
		});

		compareImplicitStats.forEach((s) => {
			if (tempImplicitStats.some((f) => f.stat === s.stat)) {
				return;
			}

			outTempImplictStats.push({
				stat: s.stat,
				value: `-${s.value}`,
			});
		});
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			{outTempImplictStats.map(({ stat, value }, i) => {
				const statName = translateStatName(stat);

				// TODO refactor how translating +123 stats are translated
				const prettyValue = translateStatValue(stat, value);

				let rowClass = '';

				// TODO should we be checking doCompare here too?
				if (compareItem) {
					if (prettyValue.indexOf('-') > -1) {
						rowClass = styles.loseStat;
					} else if (prettyValue.indexOf('+') > -1) {
						rowClass = styles.gainStat;
					}
				}

				return (
					<div key={i} className={rowClass}>
						{prettyValue} {statName}
					</div>
				);
			})}
		</>
	);
};

// TODO refactor
const VStats: Validator = ({ item, compareItem, doCompare }) => {
	const tempStats = merge({}, item.stats ?? {});
	const enchantedStats = item.enchantedStats ?? {};
	const outTempStats: { [key: string]: string } = {};

	Object.entries(tempStats).forEach(([k, v]) => {
		outTempStats[k] = '' + v;
	});

	if (compareItem && doCompare) {
		if (!item.eq) {
			const compareStats = compareItem.stats;

			Object.entries(tempStats).map(([stat, value]) => {
				if (compareStats[stat]) {
					// TODO This delta => +/- 123 could be extracted and reused
					const delta = value - compareStats[stat];
					if (delta > 0) {
						outTempStats[stat] = '+' + delta;
					} else if (delta < 0) {
						outTempStats[stat] = '' + delta;
					}
				} else {
					outTempStats[stat] = '+' + value;
				}
			});

			for (const s in compareStats) {
				if (!tempStats[s]) {
					outTempStats[s] = '' + -compareStats[s];
				}
			}
		}
	} else {
		// TODO clean this up
		// clean this all up
		// im gonna be sick
		Object.keys(outTempStats).forEach((s) => {
			if (enchantedStats[s]) {
				outTempStats[s] = (
					parseInt(outTempStats[s]) - enchantedStats[s]
				).toString();
				if (parseInt(outTempStats[s]) <= 0) {
					delete outTempStats[s];
				}

				outTempStats['_' + s] = '' + enchantedStats[s];
			}
		});
	}

	const inner = Object.keys(outTempStats)
		.map((s) => {
			const isEnchanted = s[0] === '_';
			let statName = s;
			if (isEnchanted) {
				statName = statName.substr(1);
			}

			const prettyValue = translateStatValue(statName, outTempStats[s]);
			statName = translateStatName(statName);

			let rowClass = '';

			// TODO do we need this check, and should it have doCompare too?
			if (compareItem) {
				if (prettyValue.indexOf('-') > -1) {
					rowClass = styles.loseStat;
				} else if (prettyValue.indexOf('+') > -1) {
					rowClass = styles.gainStat;
				}
			}
			if (isEnchanted) {
				rowClass += ' ';
				rowClass += styles.enchanted;
			}

			return { rowClass, prettyValue, statName };
		})
		.sort((a, b) => {
			return (
				(a.prettyValue + ' ' + a.statName).length -
				(b.prettyValue + ' ' + b.statName).length
			);
		})
		.sort((a, b) => {
			return (
				Number(a.rowClass.indexOf('enchanted') > -1) -
				Number(b.rowClass.indexOf('enchanted') > -1)
			);
		})
		.map((s, i) => {
			return (
				<div key={i} className={s.rowClass}>
					{s.prettyValue} {s.statName}
				</div>
			);
		});

	return inner.length > 0;
};
// TODO: test comparisons
const GStats: React.FC = () => {
	const { item, compareItem, doCompare } = useContext(ItemTooltipContext);

	const tempStats = merge({}, item.stats ?? {});
	const enchantedStats = item.enchantedStats ?? {};
	const outTempStats: { [key: string]: string } = {};

	Object.entries(tempStats).forEach(([k, v]) => {
		outTempStats[k] = v.toString();
	});

	if (compareItem && doCompare) {
		if (!item.eq) {
			const compareStats = compareItem.stats;

			Object.entries(tempStats).map(([stat, value]) => {
				if (compareStats[stat]) {
					// TODO This delta => +/- 123 could be extracted and reused
					const delta = value - compareStats[stat];
					if (delta > 0) {
						outTempStats[stat] = '+' + delta;
					} else if (delta < 0) {
						outTempStats[stat] = '' + delta;
					}
				} else {
					outTempStats[stat] = '+' + value;
				}
			});

			for (const s in compareStats) {
				if (!tempStats[s]) {
					outTempStats[s] = '' + -compareStats[s];
				}
			}
		}
	} else {
		// TODO clean this up
		// clean this all up
		// im gonna be sick
		Object.keys(outTempStats).forEach((s) => {
			if (enchantedStats[s]) {
				outTempStats[s] = (
					parseInt(outTempStats[s], 10) - enchantedStats[s]
				).toString();
				if (parseInt(outTempStats[s], 10) <= 0) {
					delete outTempStats[s];
				}

				outTempStats[`_${s}`] = enchantedStats[s].toString();
			}
		});
	}

	const inner = Object.keys(outTempStats)
		// Process info about each stat line
		.map((s) => {
			const isEnchanted = s[0] === '_';
			const originalName = s;
			let statName = s;
			if (isEnchanted) {
				statName = statName.substr(1);
			}

			const prettyValue = translateStatValue(statName, outTempStats[s]);
			statName = translateStatName(statName);

			let rowClass = '';

			// TODO do we need this check, and should it have doCompare too?
			if (compareItem) {
				if (prettyValue.indexOf('-') > -1) {
					rowClass = styles.loseStat;
				} else if (prettyValue.indexOf('+') > -1) {
					rowClass = styles.gainStat;
				}
			}
			if (isEnchanted) {
				rowClass += ' ';
				rowClass += styles.enchanted;
			}

			return { originalName, rowClass, prettyValue, statName };
		})
		// Sort by final display length
		.sort(
			(a, b) =>
				`${a.prettyValue} ${a.statName}`.length -
				`${b.prettyValue} ${b.statName}`.length
		)
		// Sort enchanted to the top
		.sort(
			(a, b) =>
				Number(a.rowClass.indexOf(styles.enchanted) > -1) -
				Number(b.rowClass.indexOf(styles.enchanted) > -1)
		)
		// Map to elements
		.map((s) => (
			<div
				key={s.originalName}
				className={s.rowClass}
			>{`${s.prettyValue} ${s.statName}`}</div>
		));

	if (inner.length < 1) {
		return null;
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			<GDiv className={styles.line}> </GDiv>
			<GDiv className={styles.smallSpace}> </GDiv>
			{inner}
			<GDiv className={styles.smallSpace}> </GDiv>
			<GDiv className={styles.line}> </GDiv>
		</>
	);
};

const VEffects: Validator = ({ item }) => {
	return !(
		!item.effects ||
		!item.effects.length ||
		!item.effects[0].text ||
		item.type === 'mtx'
	);
};
const GEffects: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (
		!item.effects ||
		!item.effects.length ||
		!item.effects[0].text ||
		item.type === 'mtx'
	) {
		return null;
	}

	return (
		<>
			{item.effects.map((e, i) => {
				if (i < item.effects.length - 1) {
					return (
						<span key={i}>
							{e.text}
							{/*<br />*/}
						</span>
					);
				} else {
					return <span key={i}>{e.text}</span>;
				}
			})}
		</>
	);
};

const VMaterial: Validator = ({ item }) => {
	return item.material;
};
const GMaterial: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	return item.material ? <>crafting material</> : null;
};

const VQuest: Validator = ({ item }) => {
	return item.quest;
};
const GQuest: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	return item.quest ? <>quest item</> : null;
};

const VSpellName: Validator = ({ item }) => {
	return !(!item.spell || item.ability);
};
const GSpellName: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.spell || item.ability) {
		return null;
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			<GDiv
				className={classNames(
					styles.spellName,
					styles[`q${item.spell.quality}`]
				)}
			>
				{item.spell.name}
			</GDiv>
		</>
	);
};

const VDamage: Validator = ({ item }) => {
	return !(!item.spell || !item.spell.values);
};
const GDamage: React.FC = () => {
	const { item, compareItem, doCompare } = useContext(ItemTooltipContext);

	if (!item.spell || !item.spell.values) {
		return null;
	}

	return (
		<>
			{Object.entries(item.spell.values).map(([k, v], i) => {
				if (!compareItem || !doCompare) {
					return <span key={i}>{`${k}: ${v}`}</span>;
				}

				let delta = v - compareItem.spell.values[k];
				// quote:
				// https://gitlab.com/Isleward/isleward/-/blob/cc884db87555730e29e764e9100eb11b1114af79/src/client/ui/templates/tooltipItem/buildTooltip/lineBuilders.js#L250
				//     adjust by EPSILON to handle float point imprecision, otherwise 3.15 - 2 = 1.14 or 2 - 3.15 = -1.14
				//     have to move away from zero by EPSILON, not a simple add
				if (delta >= 0) {
					delta += Number.EPSILON;
				} else {
					delta -= Number.EPSILON;
				}
				delta = ~~(delta * 100) / 100;

				let deltaStr = delta.toString();

				let rowClass = '';
				if (delta > 0) {
					rowClass = styles.gainDamage;
					deltaStr = '+' + deltaStr;
				} else if (delta < 0) {
					rowClass = styles.loseDamage;
				}

				return (
					<div key={i} className={rowClass}>
						{k}: {deltaStr}
					</div>
				);
			})}
		</>
	);
};

const VRequires: Validator = ({ item }) => {
	return !(
		!item.requires &&
		!item.level &&
		(!item.factions || !item.factions.length)
	);
};
const GRequires: React.FC<{ className: string }> = ({ className }) => {
	const { item } = useContext(ItemTooltipContext);

	if (
		!item.requires &&
		!item.level &&
		(!item.factions || !item.factions.length)
	) {
		return null;
	}

	// TODO equiperrors
	// if (equipErrors.length) {
	// 	className += ' high-level';
	// }

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			<GDiv className={className}>requires</GDiv>
		</>
	);
};

const VRequireLevel: Validator = ({ item }) => {
	return !!item.level;
};
const GRequireLevel: React.FC<{ className: string }> = ({ className }) => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.level) {
		return null;
	}

	// // TODO
	// if (equipErrors.includes('level')) {
	// 	className += ' high-level';
	// }

	// TODO handle the case where level is an array? the linebuilders source checks it its an array to show a range
	const { level } = item;

	return <GDiv className={className}>{`level: ${level}`}</GDiv>;
};

const VRequireStats: Validator = ({ item }) => {
	return !!(item.requires && item.requires[0]);
};
const GRequireStats: React.FC<{ className: string }> = ({ className }) => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.requires || !item.requires[0]) {
		return null;
	}

	// // TODO
	// if (equipErrors.includes('stats')) {
	// 	className += ' high-level';
	// }

	return (
		<GDiv className={className}>
			{item.requires[0].stat}: {item.requires[0].value}
		</GDiv>
	);
};

const VRequireFaction: Validator = ({ item }) => {
	return !!item.factions;
};
const GRequireFaction: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.factions) {
		return null;
	}

	return (
		<>
			{item.factions.map((f, i) => {
				let fstr = <>f.name + ': ' + f.tierName</>;
				if (f.noEquip) {
					// TODO check that this is working
					fstr = <span className={styles['color-red']}>{fstr}</span>;
				}
			})}
		</>
	);
};

const VWorth: Validator = ({ item }) => !!item.worthText;
const GWorth: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.worthText) {
		return null;
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			{`value: ${item.worthText}`}
		</>
	);
};

const VInfo: Validator = ({ item, doCompare, compareItem }) => {
	return !!item.slot && !!(!doCompare && compareItem);
};
const GInfo: React.FC = () => {
	const { item, compareItem, doCompare } = useContext(ItemTooltipContext);

	if (!item.slot) {
		return null;
	}

	let text = '';

	if (!doCompare && compareItem) {
		text = '[shift] to compare';
	}

	if (!text || text === '') {
		return null;
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			{text}
		</>
	);
};

const VDescription: Validator = ({ item }) => {
	return !!item.description;
};
const GDescription: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.description) {
		return null;
	}

	return (
		<>
			<GDiv className={styles.space}> </GDiv>
			{item.description}
		</>
	);
};

const VCd: Validator = ({ item }) => !!item.cd;
const GCd: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.cd) {
		return null;
	}

	return <>{`cooldown: ${item.cd}`}</>;
};

const VUses: Validator = ({ item }) => {
	return !!item.uses;
};
const GUses: React.FC = () => {
	const { item } = useContext(ItemTooltipContext);

	if (!item.uses) {
		return null;
	}

	return <>{`uses: ${item.uses}`}</>;
};

const ItemTooltip: React.FC<ItemTooltipProps> = ({
	item,
	pos = { x: 0, y: 0 },
}) => {
	// shiftdown
	// equipitemerrors
	// getcompareitem (context)

	// initLineBuilders

	const ref = useRef<HTMLDivElement>(null);

	const [usePos, setUsePos] = useState(pos);

	useLayoutEffect(() => {
		if (!ref.current) return;

		// pos is the mouse position
		const newPos = { x: pos.x + 32, y: pos.y };
		const rect = ref.current.getBoundingClientRect();

		// Check if tooltip is too close to the edge of the window
		if (newPos.x + rect.width + 8 > window.innerWidth) {
			newPos.x = pos.x - rect.width - 16;
		}
		if (newPos.y + rect.height + 8 > window.innerHeight) {
			newPos.y = pos.y - rect.height;
		}
		setUsePos(newPos);
	}, [pos, ref?.current]);

	const ctx = {
		item,
		compareItem: null,
		doCompare: false,
	};

	const containerStyle: CSSProperties = {
		left: `${usePos.x}px`,
		top: `${usePos.y}px`,
	};

	return (
		<div ref={ref} className={styles.ItemTooltip} style={containerStyle}>
			<ItemTooltipContext.Provider value={ctx}>
				<GDiv
					validate={VName}
					className={classNames(
						styles.name,
						styles[`q${item.quality}`]
					)}
				>
					<GName />
				</GDiv>
				<GDiv validate={VType} className={styles.type}>
					<GType />
				</GDiv>
				<GDiv validate={VPower} className={styles.power}>
					<GPower />
				</GDiv>
				<GDiv
					validate={VImplicitStats}
					className={styles.implicitStats}
				>
					<GImplicitStats />
				</GDiv>
				<GDiv validate={VStats} className={styles.stats}>
					<GStats />
				</GDiv>
				<GDiv validate={VMaterial} className={styles.material}>
					<GMaterial />
				</GDiv>
				<GDiv validate={VQuest} className={styles.quest}>
					<GQuest />
				</GDiv>
				<GSpellName />
				<GDiv validate={VDamage} className={styles.damage}>
					<GDamage />
				</GDiv>
				<GDiv validate={VEffects} className={styles.effects}>
					<GEffects />
				</GDiv>
				<GDiv validate={VCd} className={styles.cd}>
					<GCd />
				</GDiv>
				<GDiv validate={VUses} className={styles.uses}>
					<GUses />
				</GDiv>
				<GDiv validate={VDescription} className={styles.description}>
					<GDescription />
				</GDiv>
				<GDiv validate={VWorth} className={styles.worth}>
					<GWorth />
				</GDiv>
				<GRequires className={styles.requires} />
				<GRequireLevel className={styles.level} />
				<GRequireStats className={styles.stats} />
				<GRequireFaction />
				<GDiv validate={VInfo} className={styles.info}>
					<GInfo />
				</GDiv>
			</ItemTooltipContext.Provider>
		</div>
	);
};

export default ItemTooltip;
