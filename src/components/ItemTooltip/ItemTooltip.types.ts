import { IWDItem } from '../../isleward';

export interface ItemTooltipProps {
	item: IWDItem;
	pos?: { x: number, y: number };
	canCompare?: boolean;
}

export interface ItemTooltipContextProps {
	item: IWDItem,
	compareItem: IWDItem,
	doCompare: boolean,
	// equiErrors:
}
