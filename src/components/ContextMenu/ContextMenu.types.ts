import React from 'react';
import { CheckboxProps } from '../Checkbox/Checkbox.types';

export interface ContextMenuWrapperProps {
	children: React.ReactNode;
	contextMenu?: React.ReactNode;
}

// Provides context menu
export interface ContextMenuProps {
	pos?: { x: number, y: number },
	embedded?: boolean,
	children?: React.ReactNode
}

// Passed to CM event handlers
export interface CMController {
	close: () => void;
}

// Syntactic
/*
checkbox = <CM>
	<CM.Label>asdf</CM.Label>
</CM>
*/

export interface CMProps {
	children?: React.ReactNode,
	_controller?: CMController,
}

export interface CMLabelProps {
	children?: React.ReactNode
}

export interface CMCheckboxProps extends CheckboxProps {}

export interface CMButtonProps {
	children?: React.ReactNode
	onClick?: (CMController: CMController) => void
	delta?: number
	_controller?: CMController,
}

export interface CMExpandProps {
	label?: string,
	children?: React.ReactNode
	_controller?: CMController,
}
