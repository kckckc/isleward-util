import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';

import { CM, ContextMenu } from '.';

describe('ContextMenu', () => {
	
	it('renders correctly when floating', async () => {
		const cpn = render(<ContextMenu pos={{x: 100, y: 100}} embedded={false}>
			<CM>
				<CM.Label>my floating label text</CM.Label>
			</CM>
		</ContextMenu>);

		expect(screen.getByText('my floating label text')).toBeInTheDocument()
		expect(cpn.asFragment()).toMatchSnapshot();
	});
	it('renders correctly when embedded', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Label>my embedded label text</CM.Label>
			</CM>
		</ContextMenu>);

		expect(screen.getByText('my embedded label text')).toBeInTheDocument()
		expect(cpn.asFragment()).toMatchSnapshot();
	});

	it('renders labels', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Label>some label text</CM.Label>
			</CM>
		</ContextMenu>);

		expect(screen.getByText('some label text')).toBeInTheDocument()
	});

	it('renders checkboxes', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Checkbox checked={true}>some checkbox text</CM.Checkbox>
			</CM>
		</ContextMenu>);

		expect(screen.getByText('some checkbox text')).toBeInTheDocument()
		expect(screen.getByRole('checkbox')).toBeInTheDocument()
	});

	it('renders buttons', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Button>some button text</CM.Button>
			</CM>
		</ContextMenu>);

		expect(screen.getByText('some button text')).toBeInTheDocument()
		expect(screen.getByRole('button')).toBeInTheDocument()
	});

	it('calls "onClick" on buttons and respects the delta', async () => {
		const onClick = jest.fn();
		
		render(<ContextMenu embedded={true}>
			<CM>
				<CM.Button onClick={onClick} delta={5}>some button text</CM.Button>
			</CM>
		</ContextMenu>);

		// click, but move mouse far
		fireEvent.mouseDown(screen.getByRole('button'), { clientX: 10, clientY: 10 });
		fireEvent.mouseUp(screen.getByRole('button'), { clientX: 50, clientY: 50 });
		expect(onClick).not.toHaveBeenCalled();

		// click but make sure to be in delta
		fireEvent.mouseDown(screen.getByRole('button'), { clientX: 10, clientY: 10 });
		fireEvent.mouseUp(screen.getByRole('button'), { clientX: 10, clientY: 10 });
		expect(onClick).toHaveBeenCalled();
	});

	it('renders expander labels', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Expand label='some expander label' />
			</CM>
		</ContextMenu>);

		expect(screen.getByText('some expander label')).toBeInTheDocument()
	});

	it('renders expander content when hovered', async () => {
		const cpn = render(<ContextMenu embedded={true}>
			<CM>
				<CM.Expand label='expander label'>
					<CM.Button>inner button text</CM.Button>
				</CM.Expand>
			</CM>
		</ContextMenu>);

		// Should not be visible yet
		expect(screen.getByText('inner button text')).not.toBeVisible();
		
		// Hover the label
		let label = screen.getByText('expander label');
		expect(label).toBeVisible();
		fireEvent.mouseEnter(label);

		// Should now be visible
		expect(screen.getByText('inner button text')).toBeVisible();
		
		// Unhover
		fireEvent.mouseLeave(label);
		expect(screen.getByText('inner button text')).not.toBeVisible();
	});
	
});
