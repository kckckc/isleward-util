import React, { useEffect, useRef, useState } from 'react';

import {
	ContextMenuProps,
	ContextMenuWrapperProps,
	CMProps,
	CMLabelProps,
	CMCheckboxProps,
	CMButtonProps,
	CMExpandProps,
} from './ContextMenu.types';

import styles from './ContextMenu.module.scss';
import Checkbox from '../Checkbox';
import classNames from 'classnames';
import Portal from '../Portal';

/// Context menu container and positioning/closing logic

const ContextMenu = React.forwardRef<HTMLDivElement, ContextMenuProps>(
	({ children, pos, embedded }: ContextMenuProps, ref) => {
		if (embedded) {
			return (
				<div className={classNames(styles.CM, styles.embedded)}>
					{children}
				</div>
			);
		}

		return (
			<div
				className={styles.CM}
				style={{
					top: `${pos.y}px`,
					left: `${pos.x}px`,
				}}
				ref={ref}
				onMouseDown={(e) => e.stopPropagation()}
				onContextMenu={(e) => {
					e.stopPropagation();
					e.preventDefault();
				}}
			>
				{children}
			</div>
		);
	}
);
export { ContextMenu };

/// Elements for building context menus

const CMWrapper: React.FC<CMProps> = ({ children, _controller }) => (
	<>
		{React.Children.map(children, (child: React.ReactElement) =>
			React.cloneElement(child, {
				_controller,
			})
		)}
	</>
);

const CMLabel: React.FC<CMLabelProps> = ({ children }) => (
	<div className={classNames(styles.item, styles.label)}>{children}</div>
);

const CMCheckbox: React.FC<CMCheckboxProps> = ({
	children,
	id,
	onChange,
	delta,
	checked,
}) => (
	<div className={classNames(styles.item, styles.checkbox)}>
		<Checkbox id={id} onChange={onChange} delta={delta} checked={checked}>
			<div className={styles['checkbox-style']}>{children}</div>
		</Checkbox>
	</div>
);

const CMButton: React.FC<CMButtonProps> = ({
	children,
	onClick,
	delta = 6,
	_controller,
}) => {
	const [start, setStart] = useState({
		x: 0,
		y: 0,
	});

	const onMouseDown = (e) => {
		setStart({
			x: e.pageX ?? e.clientX,
			y: e.pageY ?? e.clientY,
		});
	};

	const onMouseUp = (e) => {
		if (e.button !== 0) return;

		const diffX = Math.abs((e.pageX ?? e.clientX) - start.x);
		const diffY = Math.abs((e.pageY ?? e.clientY) - start.y);

		if (diffX < delta && diffY < delta) {
			if (onClick) {
				onClick(_controller);
			}
		}
	};

	return (
		<div
			className={classNames(styles.item, styles.button)}
			role="button"
			tabIndex={0}
			onMouseDown={onMouseDown}
			onMouseUp={onMouseUp}
		>
			{children}
		</div>
	);
};

const CMExpand: React.FC<CMExpandProps> = ({
	label,
	children,
	_controller,
}) => {
	const [isOpen, setOpen] = useState(false);

	const onMouseEnter = () => {
		setOpen(true);
	};
	const onMouseLeave = () => {
		setOpen(false);
	};

	return (
		<div
			className={classNames(styles.item, styles.expand)}
			onMouseEnter={onMouseEnter}
			onMouseLeave={onMouseLeave}
		>
			<span className={styles.label}>{label}</span>

			<span className={styles.arrow}>&gt;</span>

			<div
				className={classNames(styles.CM, styles.content)}
				style={{ display: isOpen ? 'block' : 'none' }}
			>
				{React.Children.map(children, (child: React.ReactElement) =>
					React.cloneElement(child, {
						_controller,
					})
				)}
			</div>
		</div>
	);
};

// Add some properties to the CM wrapper for convenience
export const CM = Object.assign(CMWrapper, {
	Label: CMLabel,
	Checkbox: CMCheckbox,
	Button: CMButton,
	Expand: CMExpand,
});

// Allows rightclicking its children to open a context menu
const ContextMenuWrapper: React.FC<ContextMenuWrapperProps> = ({
	children,
	contextMenu,
}) => {
	const [pos, setPos] = useState({
		x: 0,
		y: 0,
	});

	const [showMenu, setShowMenu] = useState(false);

	const ref = useRef(null);

	// Right-clicking the wrapped element should open the context menu
	const onContextMenu = (e) => {
		// if (!contextMenu) return;

		setPos({
			x: e.pageX,
			y: e.pageY,
		});
		setShowMenu(true);

		e.preventDefault();
	};

	// Clicking outside of the context menu should close it
	useEffect(() => {
		const onClick = (e) => {
			// If the context menu is visible and we click outside of it, close
			if (showMenu && ref.current && !ref.current.contains(e.target)) {
				setShowMenu(false);
			}
		};

		// Close when scrolling too
		const onScroll = () => {
			if (showMenu) {
				setShowMenu(false);
			}
		};

		// Add and remove click handler from whole doc
		if (showMenu) {
			document.addEventListener('mousedown', onClick);
			document.addEventListener('scroll', onScroll);
		}
		return () => {
			document.removeEventListener('mousedown', onClick);
			document.removeEventListener('scroll', onScroll);
		};
	}, [showMenu, ref]);

	const closeContextMenu = () => setShowMenu(false);

	const controller = {
		close: closeContextMenu,
	};

	return (
		<>
			{React.Children.map(children, (child: React.ReactElement) =>
				React.cloneElement(child, {
					onContextMenu,
				})
			)}

			{/* If there is a visible context menu, render it */}
			{showMenu && contextMenu && (
				<Portal>
					<ContextMenu ref={ref} pos={pos}>
						{React.Children.map(
							contextMenu,
							(child: React.ReactElement) =>
								React.cloneElement(child, {
									_controller: controller,
								})
						)}
					</ContextMenu>
				</Portal>
			)}
		</>
	);
};
export { ContextMenuWrapper };
