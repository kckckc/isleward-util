import React from 'react';
import { Meta, Story } from '@storybook/react';

import { ContextMenuWrapper, ContextMenu, CM } from './ContextMenu';
import { ContextMenuWrapperProps, ContextMenuProps } from './ContextMenu.types';

import styles from '../stories.module.scss';

export default {
	title: 'ContextMenu',
	component: ContextMenu,
	parameters: {
		backgrounds: {
			default: 'ingame',
		},
	},
} as Meta;

const ProviderTemplate: Story<ContextMenuWrapperProps> = (args) => (
	<ContextMenuWrapper {...args}>
		<div className={styles.StoryPanel}>right click me for context</div>
	</ContextMenuWrapper>
);

const InnerTemplate = () => {
	return (
		<CM>
			<CM.Label>label 1</CM.Label>
			<CM.Checkbox id="check1" checked={true}>
				checkbox 1
			</CM.Checkbox>
			<CM.Checkbox id="check2" checked={false}>
				checkbox 2
			</CM.Checkbox>
			<CM.Button>button 1</CM.Button>
			<CM.Button>button 2</CM.Button>
			<CM.Expand label="expander">
				<CM.Label>label 2</CM.Label>
				<CM.Checkbox id="check3" checked={true}>
					checkbox 3
				</CM.Checkbox>
				<CM.Checkbox id="check4" checked={false}>
					checkbox 4
				</CM.Checkbox>
				<CM.Button>button 3</CM.Button>
				<CM.Button>button 4</CM.Button>
			</CM.Expand>
		</CM>
	);
};

export const Default: Story<ContextMenuWrapperProps> = ProviderTemplate.bind(
	{}
);
Default.args = {
	contextMenu: InnerTemplate(),
};

export const AbsolutelyPositioned: Story<ContextMenuWrapperProps> = (args) => (
	<div
		style={{
			position: 'absolute',
			left: '300px',
			top: '300px',
		}}
	>
		<ContextMenuWrapper {...args}>
			<div className={styles.StoryPanel}>right click me for context</div>
		</ContextMenuWrapper>
	</div>
);
AbsolutelyPositioned.args = {
	contextMenu: InnerTemplate(),
};

export const Closeable: Story<ContextMenuWrapperProps> = ProviderTemplate.bind(
	{}
);
Closeable.args = {
	contextMenu: (
		<CM>
			<CM.Label>label 1</CM.Label>
			<CM.Checkbox id="check1" checked={true}>
				checkbox 1
			</CM.Checkbox>
			<CM.Checkbox id="check2" checked={false}>
				checkbox 2
			</CM.Checkbox>
			<CM.Button onClick={(e) => e.close()}>close 1</CM.Button>
			<CM.Button onClick={(e) => e.close()}>close 2</CM.Button>
			<CM.Expand label="expander">
				<CM.Label>label 2</CM.Label>
				<CM.Checkbox id="check3" checked={true}>
					checkbox 3
				</CM.Checkbox>
				<CM.Checkbox id="check4" checked={false}>
					checkbox 4
				</CM.Checkbox>
				<CM.Button onClick={(e) => e.close()}>close 3</CM.Button>
				<CM.Button onClick={(e) => e.close()}>close 4</CM.Button>
			</CM.Expand>
		</CM>
	),
};

const EmbeddedTemplate: Story<ContextMenuProps> = (args) => (
	<div className={styles.StoryPanel}>
		<ContextMenu embedded={true}>
			<InnerTemplate />
		</ContextMenu>
	</div>
);

export const Embedded: Story<ContextMenuProps> = EmbeddedTemplate.bind({});

const Expand2Template = () => {
	return (
		<>
			<CM.Expand label="expand">
				<CM.Label>label</CM.Label>
				<CM.Checkbox id="check5" checked={true}>
					checkbox 1
				</CM.Checkbox>
				<CM.Checkbox id="check6" checked={false}>
					checkbox 2
				</CM.Checkbox>
				<CM.Button>button 1</CM.Button>
				<CM.Button>button 2</CM.Button>
			</CM.Expand>
		</>
	);
};
const ExpandTemplate = () => {
	return (
		<>
			<CM.Expand label="expand">
				<Expand2Template />
				<Expand2Template />
				<Expand2Template />
				<Expand2Template />
				<Expand2Template />
			</CM.Expand>
		</>
	);
};

export const Expand: Story<ContextMenuWrapperProps> = ProviderTemplate.bind({});
Expand.args = {
	contextMenu: (
		<CM>
			<ExpandTemplate />
			<ExpandTemplate />
			<ExpandTemplate />
			<ExpandTemplate />
			<ExpandTemplate />
		</CM>
	),
};
