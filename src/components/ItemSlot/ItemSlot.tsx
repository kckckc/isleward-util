import React, { useState } from 'react';

import { ItemSlotProps } from './ItemSlot.types';

import styles from './ItemSlot.module.scss';
import ItemImage from '../ItemImage';
import ItemTooltip from '../ItemTooltip';
import { ContextMenuWrapper } from '../ContextMenu';
import Portal from '../Portal';

const ItemSlot: React.FC<ItemSlotProps> = ({
	item,
	tooltip = true,
	contextMenu,
	slot,
	usable,
	quantity,
	onClick,
	selected = false,
	noBackground = false,
	dropShadow = false,
}) => {
	const [pos, setPos] = useState({ x: 0, y: 0 });

	const [showTooltip, setShowTooltip] = useState(false);

	const onMouseEnter = () => {
		setShowTooltip(true);
	};
	const onMouseLeave = () => {
		setShowTooltip(false);
	};
	const onMouseMove = (e: React.MouseEvent) => {
		setPos({
			x: Math.floor(e.pageX),
			y: Math.floor(e.pageY),
		});
	};

	const clickProps: {
		onClick?: () => void;
		role?: string;
		tabIndex?: number;
		onKeyDown?: (KeyboardEvent) => void;
	} = {
		onClick,
	};
	if (onClick) {
		clickProps.tabIndex = 0;
		clickProps.role = 'button';
		clickProps.onKeyDown = (e) => {
			if (e.key === 'Enter' || e.key === 'Space') {
				onClick();
			}
		};
	}

	return (
		<ContextMenuWrapper contextMenu={contextMenu}>
			<div
				className={styles.ItemSlot}
				onMouseEnter={onMouseEnter}
				onMouseLeave={onMouseLeave}
				onMouseMove={onMouseMove}
				{...clickProps}
			>
				<ItemImage
					item={item}
					slot={slot}
					usable={usable}
					quantity={quantity}
					selected={selected}
					noBackground={noBackground}
					dropShadow={dropShadow}
				/>

				{showTooltip && tooltip && item && (
					<Portal>
						<ItemTooltip item={item} pos={pos} />
					</Portal>
				)}
			</div>
		</ContextMenuWrapper>
	);
};

export default ItemSlot;
