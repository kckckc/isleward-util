import Autocomplete from './Autocomplete';
import Button from './Button';
import Checkbox from './Checkbox';
import { CM, ContextMenu, ContextMenuWrapper } from './ContextMenu';
import FilterReference from './FilterReference';
import Input from './Input';
import ItemImage from './ItemImage';
import ItemPreference from './ItemPreference';
import ItemSlot from './ItemSlot';
import ItemTooltip from './ItemTooltip';
import Panel from './Panel';
import PanelOrder from './PanelOrder';
import Portal from './Portal';

export type SlotType =
	| 'head'
	| 'neck'
	| 'chest'
	| 'hands'
	| 'finger-1'
	| 'finger-2'
	| 'finger'
	| 'waist'
	| 'legs'
	| 'feet'
	| 'trinket'
	| 'oneHanded'
	| 'offHand'
	| 'tool'
	| 'rune'
	| 'quick';

export {
	Autocomplete,
	Button,
	Checkbox,
	ContextMenuWrapper,
	ContextMenu,
	CM,
	FilterReference,
	Input,
	ItemImage,
	ItemPreference,
	ItemSlot,
	ItemTooltip,
	Panel,
	PanelOrder,
	Portal,
};
