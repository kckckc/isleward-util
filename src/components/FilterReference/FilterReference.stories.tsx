import React from 'react';
import { Meta, Story } from '@storybook/react';
import FilterReference from './FilterReference';
import { FilterReferenceProps } from './FilterReference.types';

export default {
	title: 'FilterReference',
	component: FilterReference
} as Meta;

const Template: Story<FilterReferenceProps> = (args) => <FilterReference {...args} />;

export const Default: Story<FilterReferenceProps> = Template.bind({});
Default.args = {}
