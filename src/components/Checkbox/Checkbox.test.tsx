import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { composeStories } from '@storybook/testing-react';

import * as stories from './Checkbox.stories';

const { Default, Checked, Unchecked } = composeStories(stories);

describe('Checkbox', () => {
	it('provides aria-checked if checked', () => {
		render(<Checked />);

		const checkbox = screen.getByRole('checkbox');
		expect(checkbox.getAttribute('aria-checked')).toBe('true');
	});
	it('does not provide aria-checked if not checked', () => {
		render(<Unchecked />);

		const checkbox = screen.getByRole('checkbox');
		expect(checkbox.getAttribute('aria-checked')).not.toBe('true');
	});

	it('calls "onChange" when clicked', () => {
		const onChange = jest.fn();

		render(<Default onChange={onChange} />);

		// clicking label should not fire
		fireEvent.mouseDown(screen.getByText('Checkbox'));
		fireEvent.mouseUp(screen.getByText('Checkbox'));
		expect(onChange).not.toHaveBeenCalled();

		// clicking box should fire
		fireEvent.mouseDown(screen.getByRole('checkbox'));
		fireEvent.mouseUp(screen.getByRole('checkbox'));
		expect(onChange).toHaveBeenCalled();
	});

	it('ignores clicks where the mouse moved more than the delta', () => {
		const onChange = jest.fn();

		render(<Default onChange={onChange} />);

		// click, but move mouse far
		fireEvent.mouseDown(screen.getByRole('checkbox'), { clientX: 10, clientY: 10 });
		fireEvent.mouseUp(screen.getByRole('checkbox'), { clientX: 50, clientY: 50 });
		expect(onChange).not.toHaveBeenCalled();

		// click but make sure to be in delta
		fireEvent.mouseDown(screen.getByRole('checkbox'), { clientX: 10, clientY: 10 });
		fireEvent.mouseUp(screen.getByRole('checkbox'), { clientX: 10, clientY: 10 });
		expect(onChange).toHaveBeenCalled();
	});

});
