import React, { useState } from 'react';
import { nanoid } from 'nanoid';

import { CheckboxProps } from './Checkbox.types';

import styles from './Checkbox.module.scss';
import classNames from 'classnames';

type IStart = {
	x: number;
	y: number;
};

const Checkbox: React.FC<CheckboxProps> = ({
	checked = false,
	id,
	children,
	onChange,
	delta = 6,
}) => {
	// Unique ID for label
	const [useId] = useState(id || nanoid());

	// Delta check
	const [start, setStart] = useState<IStart>({ x: 0, y: 0 } as IStart);

	const onMouseDown = (e) => {
		setStart({
			x: e.pageX ?? e.clientX,
			y: e.pageY ?? e.clientY,
		});
	};
	const onMouseUp = (e) => {
		if (e.button !== 0) return;

		// Check the distance the mouse moved
		const diffX = Math.abs((e.pageX ?? e.clientX) - start.x);
		const diffY = Math.abs((e.pageY ?? e.clientY) - start.y);

		// If we moved far, don't register a click
		if (diffX < delta && diffY < delta) {
			if (onChange) {
				onChange();
			}
		}
	};
	const onKeyDown = (e: React.KeyboardEvent) => {
		if (e.key === ' ' || e.key === 'Enter') {
			if (onChange) {
				onChange();
			}

			e.stopPropagation();
			e.preventDefault();
		}
	};

	return (
		<div className={styles.Checkbox}>
			<div
				className={classNames(
					styles.checkmark,
					checked && styles.checked
				)}
				role="checkbox"
				tabIndex={0}
				onMouseDown={onMouseDown}
				onMouseUp={onMouseUp}
				onKeyDown={onKeyDown}
				aria-checked={checked}
				aria-labelledby={`label-${useId}`}
				id={`checkbox-${useId}`}
			/>

			<label
				className={styles.label}
				id={`label-${useId}`}
				htmlFor={`checkbox-${useId}`}
			>
				{children}
			</label>
		</div>
	);
};

export default Checkbox;
