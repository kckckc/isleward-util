export interface ItemPreferenceProps {
	qualityIndicators?: 'off' | 'border' | 'bottom' | 'background'
	unusableIndicators?: 'off' | 'border' | 'top' | 'background'
}
