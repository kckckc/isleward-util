import { createContext } from 'react';

import { ItemPreferenceProps } from './ItemPreference.types';

const ItemPreference = createContext<ItemPreferenceProps>({
	qualityIndicators: 'off',
	unusableIndicators: 'off',
});

export default ItemPreference;
