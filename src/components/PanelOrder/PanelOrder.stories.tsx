import { forceReRender, Meta, Story } from '@storybook/react';
import React, { useState } from 'react';
import Panel from '../Panel/Panel';
import { PanelProps } from '../Panel/Panel.types';
import PanelOrder from './PanelOrder';

export default {
	title: 'PanelOrder',
	component: Panel,
} as Meta;

const ProviderTemplate: React.FC = ({ children }) => {
	const [value, setValue] = useState(40);
	return (
		<PanelOrder.Provider value={{ value, setValue }}>
			{children}
		</PanelOrder.Provider>
	);
};

const Template: Story<PanelProps> = (args) => {
	return (
		<ProviderTemplate>
			<Panel {...args} />
			<Panel {...args} />
			<Panel {...args} />
		</ProviderTemplate>
	);
};

export const Default: Story<PanelProps> = Template.bind({});
Default.args = {
	children: 'body text',
	header: 'header',
};

export const WithSave: Story<PanelProps> = (args) => {
	return (
		<ProviderTemplate>
			<Panel saveAs={'orderstory1'} {...args} />
			<Panel saveAs={'orderstory2'} {...args} />
			<Panel saveAs={'orderstory3'} {...args} />
		</ProviderTemplate>
	);
};
WithSave.args = {
	children: <div style={{ margin: '16px' }}>body text</div>,
	header: 'header',
};

// export const OtherExample: Story<PanelOrderProps> = Template.bind({});
// OtherExample.args = {
// 	children: 'bar'
// }
