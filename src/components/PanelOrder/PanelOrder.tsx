import { createContext } from 'react';

import { PanelOrderProps } from './PanelOrder.types';

const PanelOrder = createContext<PanelOrderProps>({
	value: 30,
	setValue: () => {},
});

export default PanelOrder;
