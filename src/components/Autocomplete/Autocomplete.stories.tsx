import React, { useState } from 'react';
import { Meta, Story } from '@storybook/react';
import Autocomplete from './Autocomplete';
import { AutocompleteProps } from './Autocomplete.types';

export default {
	title: 'Autocomplete',
	component: Autocomplete
} as Meta;

const options = ['vitality', 'health regeneration', 'maximum mana', 'mana regeneration', 'strength', 'intellect', 'dexterity', 'armor', 'chance to block attacks', 'chance to block spells', 'chance to dodge attacks', 'chance to dodge spells', 'global crit chance', 'global crit multiplier', 'attack crit chance', 'attack crit multiplier', 'spell crit chance', 'spell crit multiplier', 'increased item quality', 'increased item quantity', 'sprint chance', 'to all attributes', 'additional xp per kill', 'level requirement reduction', 'increased arcane damage', 'increased frost damage', 'increased fire damage', 'increased holy damage', 'increased poison damage', 'increased physical damage', 'increased elemental damage', 'increased spell damage', 'all resistance', 'arcane resistance', 'frost resistance', 'fire resistance', 'holy resistance', 'poison resistance', 'attack speed', 'cast speed', 'life gained on hit', 'aura mana reservation multiplier', 'stats', 'lb', 'extra catch chance', 'faster catch speed', 'higher fish rarity', 'increased fish weight', 'extra chance to hook items', 'maximum hp'];

const Template: Story<AutocompleteProps> = (args) => {
	const [value, setValue] = useState('');
	return <Autocomplete {...args} value={value} setValue={setValue} />
};

export const Default: Story<AutocompleteProps> = Template.bind({});
Default.args = {
	id: 'default',
	options,
	placeholder: 'type to autocomplete'
}

export const AlwaysShowOptions: Story<AutocompleteProps> = Template.bind({});
AlwaysShowOptions.args = {
	id: 'default',
	options,
	placeholder: 'type to autocomplete',
	selectMode: true
}

export const WithError: Story<AutocompleteProps> = Template.bind({});
WithError.args = {
	id: 'default',
	options,
	placeholder: 'type to autocomplete',
	indicator: '(!)',
	indicatorColor: 'yellow',
}

export const Layout: Story = () => <div>
	<p>asdasdasdasdasdasd</p>
	<div style={{
		display: 'flex',
		flexDirection: 'row',
		columnGap: '8px',
		width: '100%',
	}}>
		<div style={{
			flexGrow: 1
		}}>
			<Default id="a" options={options} placeholder='aaaaaa' />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default id="b" options={options} placeholder='bbbbbb' indicator="(!)" indicatorColor="red" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default id="c" options={options} placeholder='cccccc' indicator="(?)" indicatorColor="yellow" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default id="d" options={options} placeholder='dddddd' indicator="(i)" indicatorColor="blue" />
		</div>
		<div style={{
			flexGrow: 1
		}}>
			<Default id="e" options={options} placeholder='eeeeee' indicator="✔" indicatorColor="green" />
		</div>
	</div>
	<p>asdasdasdasdasdasd</p>
</div>
