export interface AutocompleteProps {
	id: string;
	value?: string;
	setValue?: (val: string) => void;

	onSubmit?: () => void;

	placeholder?: string;
	options?: string[];
	selectMode?: boolean;

	indicator?: string;
	indicatorColor?: 'red' | 'blue' | 'green' | 'yellow';
	indicatorTooltip?: string;

	centered?: boolean;
}
