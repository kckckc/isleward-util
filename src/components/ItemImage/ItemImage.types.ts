import { SlotType } from '..';
import { ItemQuantityFilters } from '../..';
import { IWDItem } from '../../isleward';

export interface ItemImageProps {
	item?: IWDItem;
	slot?: SlotType;
	usable?: boolean; // TODO ?
	selected?: boolean;
	quantity?: ItemQuantityFilters;
	noBackground?: boolean;
	dropShadow?: boolean;
}
