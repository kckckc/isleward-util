import React, { useContext } from 'react';

import { ItemImageProps } from './ItemImage.types';

import styles from './ItemImage.module.scss';

import ItemPreference from '../ItemPreference';
import { getItemSpriteStyle } from '../../calc/spritesheet';
import { getItemQuantity } from '../../calc/itemQuantity';
import classNames from 'classnames';

const ItemImage: React.FC<ItemImageProps> = ({
	item,
	slot,
	quantity: quantityFilters,
	usable = true,
	selected = false,
	noBackground,
	dropShadow = false,
}) => {
	const itemPref = useContext(ItemPreference);

	if (!item && !slot) {
		return (
			<div
				className={classNames(
					styles.ItemImage,
					styles.empty,
					selected && styles.selected,
					noBackground && styles.noBackground
				)}
			>
				<div className={styles.icon} />
				<div className={styles.quantity} />
			</div>
		);
	}
	if (!item) {
		return (
			<div
				className={classNames(
					styles.ItemImage,
					styles['show-default-icon'],
					selected && styles.selected,
					noBackground && styles.noBackground
				)}
				slot={slot}
			>
				<div className={styles.icon} />
				<div className={styles.quantity} />
			</div>
		);
	}

	const style = getItemSpriteStyle(item);

	const { quantity, classNames: quantityClassNames } = getItemQuantity(
		item,
		quantityFilters
	);

	// TODO drag n drop slots w/ hover highlighting
	return (
		<div
			className={classNames(
				styles.ItemImage,
				selected && styles.selected,
				item.quality && [
					styles[`quality-${itemPref.qualityIndicators}`],
					styles[`quality-${item.quality}`],
				],
				(itemPref.unusableIndicators ?? 'off') !== 'off' && [
					styles[`unusable-${itemPref.unusableIndicators}`],
					!usable && styles['no-equip'],
				],
				noBackground && styles.noBackground
			)}
		>
			<div
				className={classNames(
					styles.icon,
					dropShadow && styles.dropshadow
				)}
				style={style}
			/>
			<div className={quantityClassNames.map((q) => styles[q]).join(' ')}>
				{quantity}
			</div>
		</div>
	);
};

export default ItemImage;
