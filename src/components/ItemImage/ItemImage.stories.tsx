import React from 'react';
import { Meta, Story } from '@storybook/react';

import ItemImage from './ItemImage';
import { ItemImageProps } from './ItemImage.types';
import { SlotType } from '..';

export default {
	title: 'ItemImage',
	component: ItemImage,
} as Meta;

const Template: Story<ItemImageProps> = (args) => <ItemImage {...args} />;

let testDataRaw = `[{"enchantedStats":{"addCritMultiplier":16,"int":5},"equipSlot":"trinket","id":0,"implicitStats":[{"stat":"armor","value":70}],"level":14,"name":"Forged Ember","power":2,"quality":1,"slot":"trinket","sprite":[8,0],"stats":{"addCritMultiplier":16,"int":5,"manaMax":6,"regenHp":3},"type":"Forged Ember","worth":92,"eq":true,"pos":null},{"ability":true,"id":1,"level":20,"name":"Rune of Consecrate","quality":1,"spell":{"name":"Consecrate","rolls":{"healing":0.30372441919183246,"i_duration":0.3976776140269857},"type":"healingCircle","values":{"duration":9,"healing":0.36}},"sprite":[10,0],"worth":645,"pos":42},{"ability":true,"id":2,"level":18,"name":"Rune of Magic Missile","quality":3,"spell":{"name":"Magic Missile","rolls":{"damage":0.5941850364135666},"type":"projectile","values":{"damage":20.63}},"sprite":[10,0],"worth":1180,"eq":true,"runeSlot":1,"pos":null},{"enchantedStats":{"addSpellCritChance":38,"str":4},"equipSlot":"head","id":3,"implicitStats":[{"stat":"armor","value":109}],"level":14,"name":"Shattered Legend","power":2,"quality":3,"requires":[{"stat":"str","value":12}],"slot":"head","sprite":[0,0],"stats":{"addCritMultiplier":3,"addSpellCritChance":79,"int":10,"str":4},"type":"Helmet","worth":212,"eq":true,"pos":null},{"enchantedStats":{"itemQuantity":17,"magicFind":9,"regenMana":2},"equipSlot":"chest","id":4,"implicitStats":[{"stat":"armor","value":175}],"level":20,"name":"Argent Quest","power":3,"quality":2,"requires":[{"stat":"dex","value":24}],"slot":"chest","sprite":[2,2],"stats":{"addAttackCritMultiplier":11,"int":8,"itemQuantity":17,"magicFind":9,"regenMana":2,"str":5},"type":"Leather Armor","worth":179,"eq":true,"pos":null},{"enchantedStats":{"elementFireResist":4,"int":8,"lvlRequire":6},"equipSlot":"hands","id":5,"implicitStats":[{"stat":"armor","value":30}],"level":14,"name":"Pathfinder Testimony","originalLevel":20,"power":3,"quality":2,"requires":[{"stat":"int","value":66}],"slot":"hands","sprite":[3,1],"stats":{"addAttackCritChance":30,"elementAllResist":4,"elementFireResist":4,"int":18,"lvlRequire":6},"type":"Gloves","worth":179,"eq":true,"pos":null},{"enchantedStats":{"elementAllResist":4},"id":6,"implicitStats":[{"stat":"regenMana","value":4}],"level":20,"name":"Sanguine Silence","power":1,"quality":2,"requires":[{"stat":"int","value":16}],"slot":"twoHanded","spell":{"auto":true,"castTimeMax":0,"cdMax":7,"element":"arcane","manaCost":0,"name":"Projectile","quality":2,"random":{"damage":[1.65,10.81]},"range":9,"rolls":{"damage":0.5217262675414459},"statMult":1,"statType":"int","type":"projectile","values":{"damage":6.42}},"sprite":[9,1],"stats":{"addCritChance":58,"elementAllResist":4,"elementFrostResist":11,"int":12},"type":"Gnarled Staff","worth":179,"pos":31},{"enchantedStats":{"addSpellCritChance":50,"elementFrostPercent":3,"vit":4},"equipSlot":"finger-2","factions":[{"id":"pumpkinSailor","tier":5,"tierName":"Honored","name":"The Pumpkin Sailor","noEquip":null}],"id":7,"implicitStats":[{"stat":"allAttributes","value":8}],"level":20,"name":"Signet of Witching","noDestroy":false,"noDrop":true,"noSalvage":true,"power":3,"quality":3,"slot":"finger","sprite":[0,0],"spritesheet":"server/mods/iwd-souls-moor/images/items.png","stats":{"addSpellCritChance":50,"elementFrostPercent":3,"int":12,"regenMana":5,"vit":4},"type":"Ring","worth":0,"eq":true,"pos":null},{"enchantedStats":{"addAttackCritMultiplier":22,"elementAllResist":5,"regenMana":4},"equipSlot":"finger-1","factions":[{"id":"pumpkinSailor","tier":5,"tierName":"Honored","name":"The Pumpkin Sailor","noEquip":null}],"id":8,"implicitStats":[{"stat":"allAttributes","value":8}],"level":20,"name":"Signet of Witching","noDestroy":false,"noDrop":true,"noSalvage":true,"power":3,"quality":3,"slot":"finger","sprite":[0,0],"spritesheet":"server/mods/iwd-souls-moor/images/items.png","stats":{"addAttackCritMultiplier":22,"elementAllResist":5,"int":12,"regenMana":9},"type":"Ring","worth":0,"eq":true,"pos":null},{"chance":30,"id":9,"magicFind":0,"name":"Digested Crystal","quality":0,"quantity":200,"quest":true,"sprite":[1,1],"pos":39},{"enchantedStats":{"lvlRequire":4},"equipSlot":"neck","id":10,"implicitStats":[{"stat":"regenHp","value":3}],"level":12,"name":"Choker","originalLevel":16,"power":1,"quality":1,"slot":"neck","sprite":[1,3],"stats":{"allAttributes":8,"elementFrostPercent":2,"lvlRequire":4},"type":"Choker","worth":104,"eq":true,"pos":null},{"effects":[{"factionId":null,"text":"Reduces the mana reserved by tranquility by 20%","properties":null,"type":"reduceRuneManaReserve","rolls":{"amount":20,"rune":"tranquility"}}],"enchantedStats":{"addSpellCritChance":36,"elementFireResist":7,"int":4},"equipSlot":"waist","id":11,"level":20,"name":"Sash of Souls","power":3,"quality":3,"slot":"waist","sprite":[1,0],"spritesheet":"server/mods/iwd-souls-moor/images/items.png","stats":{"addSpellCritChance":36,"elementFireResist":7,"int":4,"regenMana":8},"type":"Sash","eq":true,"pos":null},{"ability":true,"id":12,"level":20,"name":"Rune of Consecrate","quality":2,"spell":{"name":"Consecrate","rolls":{"healing":0.46664171847083596,"i_duration":0.42022980679591454},"type":"healingCircle","values":{"healing":0.39,"duration":10}},"sprite":[10,0],"worth":895,"eq":true,"runeSlot":3,"pos":null},{"enchantedStats":{"magicFind":11,"regenHp":7,"regenMana":4},"id":13,"implicitStats":[{"stat":"armor","value":47}],"level":20,"name":"Knight Sentry","power":3,"quality":3,"requires":[{"stat":"int","value":33}],"slot":"legs","sprite":[6,1],"stats":{"elementPoisonResist":4,"int":10,"magicFind":11,"regenHp":7,"regenMana":4,"vit":5},"type":"Pants","worth":249,"pos":43},{"enchantedStats":{"addAttackCritChance":56,"addAttackCritMultiplier":13,"manaMax":4},"equipSlot":"feet","id":14,"implicitStats":[{"stat":"armor","value":23}],"level":20,"name":"Genesis Vessel","power":3,"quality":2,"requires":[{"stat":"int","value":16}],"slot":"feet","sprite":[7,1],"stats":{"addAttackCritChance":56,"addAttackCritMultiplier":13,"dodgeSpellChance":7,"int":8,"manaMax":4,"xpIncrease":5},"type":"Boots","worth":179,"eq":true,"pos":null},{"enchantedStats":{"elementAllResist":4,"lvlRequire":4,"magicFind":11},"id":15,"implicitStats":[{"stat":"armor","value":63}],"level":16,"name":"Sleeping Relic","originalLevel":20,"power":3,"quality":3,"requires":[{"stat":"dex","value":16}],"slot":"head","sprite":[0,2],"stats":{"elementAllResist":4,"elementFireResist":6,"int":7,"itemQuantity":14,"lvlRequire":4,"magicFind":11,"str":8},"type":"Leather Cap","worth":249,"pos":34},{"cd":0,"cdMax":342,"description":"Would taste better with some lime.","id":16,"infinite":true,"magicFind":2000,"name":"Djinn's Tonic","noAugment":true,"noSalvage":true,"quality":3,"sprite":[7,2],"spritesheet":"server/mods/iwd-summer-fest/images/items.png","type":"toy","pos":40},{"cd":0,"cdMax":86,"description":"Stout and dependable.","effects":[{"factionId":null,"text":null,"properties":null,"type":"mounted","rolls":{"cell":1,"sheetName":"server/mods/iwd-mounts/images/mounts.png","speed":100}}],"id":17,"name":"Gray Donkey","noSalvage":true,"quality":0,"sprite":[1,0],"spritesheet":"server/mods/iwd-mounts/images/reins.png","stats":{"sprintChance":100},"type":"mount","useText":"mount","pos":49,"quickSlot":0},{"ability":true,"id":18,"level":20,"name":"Rune of Ice Spear","quality":3,"spell":{"name":"Ice Spear","rolls":{"damage":0.45491093617506606,"i_freezeDuration":0.7527303089757251},"type":"iceSpear","values":{"damage":7.91,"freezeDuration":9}},"sprite":[10,0],"worth":1245,"eq":true,"runeSlot":4,"pos":null},{"enchantedStats":{"addAttackCritMultiplier":25,"int":1,"xpIncrease":3},"id":19,"implicitStats":[{"stat":"regenMana","value":7}],"level":20,"name":"Eternity Snare","power":3,"quality":3,"requires":[{"stat":"int","value":24}],"slot":"twoHanded","spell":{"auto":true,"castTimeMax":0,"cdMax":7,"element":"arcane","manaCost":0,"name":"Projectile","quality":3,"random":{"damage":[1.65,10.81]},"range":9,"rolls":{"damage":0.7366599612280533},"statMult":1,"statType":"int","type":"projectile","values":{"damage":8.39}},"sprite":[9,1],"stats":{"addAttackCritMultiplier":25,"addSpellCritMultiplier":58,"elementAllResist":10,"int":1,"xpIncrease":5},"type":"Gnarled Staff","worth":249,"pos":30},{"enchantedStats":{"castSpeed":8,"elementFrostPercent":3,"elementFrostResist":6},"id":20,"implicitStats":[{"stat":"int","value":2}],"level":20,"name":"Journeyman Bite","power":3,"quality":2,"slot":"neck","sprite":[1,1],"stats":{"addSpellCritChance":47,"addSpellCritMultiplier":15,"allAttributes":5,"castSpeed":8,"elementFrostPercent":3,"elementFrostResist":6},"type":"Amulet","worth":179,"pos":35},{"effects":[{"factionId":null,"text":"you take 21% of the damage you deal","properties":{"element":"poison"},"type":"damageSelf","rolls":{"percentage":21}},{"factionId":null,"text":"your hits always crit","properties":null,"type":"alwaysCrit","rolls":{}}],"enchantedStats":{"addSpellCritChance":105,"dex":6},"id":21,"level":19,"name":"Steelclaw's Bite","power":3,"quality":4,"requires":[{"stat":"dex","value":21}],"slot":"oneHanded","spell":{"castTimeMax":0,"cdMax":3,"name":"Melee","quality":0,"random":{"damage":[0.88,5.79]},"rolls":{"damage":0.16986843881843003},"statMult":1,"statType":"dex","type":"melee","useWeaponRange":true,"values":{"damage":1.71}},"sprite":[1,0],"spritesheet":"../../../images/legendaryItems.png","stats":{"addCritMultiplier":40,"addSpellCritChance":105,"dex":17,"vit":3},"type":"Curved Dagger","worth":332,"pos":37},{"enchantedStats":{"lifeOnHit":6,"magicFind":11},"equipSlot":"offHand","id":22,"implicitStats":[{"stat":"addSpellCritMultiplier","value":26}],"level":20,"name":"Scout's Tide","power":2,"quality":3,"requires":[{"stat":"int","value":28}],"slot":"offHand","sprite":[13,3],"stats":{"addCritMultiplier":16,"addSpellCritChance":32,"lifeOnHit":6,"magicFind":11,"regenHp":5,"xpIncrease":3},"type":"Ancient Tome","worth":212,"eq":true,"pos":null},{"enchantedStats":{"addSpellCritChance":113,"elementFireResist":2},"id":23,"implicitStats":[{"stat":"regenMana","value":4}],"level":20,"name":"Silent Prospect","power":2,"quality":3,"requires":[{"stat":"int","value":16}],"slot":"twoHanded","spell":{"auto":true,"castTimeMax":0,"cdMax":7,"element":"arcane","manaCost":0,"name":"Projectile","quality":1,"random":{"damage":[1.65,10.81]},"range":9,"rolls":{"damage":0.3139427933240991},"statMult":1,"statType":"int","type":"projectile","values":{"damage":4.52}},"sprite":[9,1],"stats":{"addSpellCritChance":113,"elementFireResist":2,"int":15,"itemQuantity":5,"magicFind":7,"vit":9},"type":"Gnarled Staff","worth":249,"pos":32},{"enchantedStats":{"manaMax":7,"regenHp":8,"regenMana":4},"equipSlot":"legs","id":24,"implicitStats":[{"stat":"armor","value":144}],"level":20,"name":"Courier Penance","power":3,"quality":2,"requires":[{"stat":"str","value":33}],"slot":"legs","sprite":[6,0],"stats":{"int":12,"magicFind":7,"manaMax":7,"regenHp":8,"regenMana":4},"type":"Legplates","worth":179,"eq":true,"pos":null},{"ability":true,"id":25,"level":13,"name":"Rune of Smite","quality":3,"spell":{"name":"Smite","rolls":{"damage":0.5150672560677102,"i_stunDuration":0.2745184580401292},"type":"smite","values":{"damage":9.15,"stunDuration":7}},"sprite":[10,0],"worth":1030,"eq":true,"runeSlot":2,"pos":null},{"ability":true,"id":26,"level":20,"name":"Rune of Tranquility","quality":2,"spell":{"name":"Tranquility","rolls":{"regenPercentage":0.47836094496388626},"type":"aura","values":{"regenPercentage":6.87}},"sprite":[10,0],"worth":895,"pos":41},{"enchantedStats":{"addAttackCritMultiplier":17,"addCritMultiplier":7,"elementHolyResist":2},"equipSlot":"oneHanded","id":27,"implicitStats":[{"stat":"castSpeed","value":2}],"level":20,"name":"Wand","power":3,"quality":1,"requires":[{"stat":"int","value":33}],"slot":"oneHanded","spell":{"castTimeMax":0,"cdMax":5,"element":"holy","manaCost":0,"name":"Projectile","quality":4,"random":{"damage":[1.17,7.72]},"range":9,"rolls":{"damage":0.9517386405262704},"statMult":1,"statType":"int","type":"projectile","values":{"damage":7.4}},"sprite":[9,8],"stats":{"addAttackCritMultiplier":17,"addCritMultiplier":7,"elementHolyResist":2,"int":4,"magicFind":7},"type":"Wand","worth":129,"eq":true,"runeSlot":0,"pos":null},{"enchantedStats":{"lvlRequire":7,"magicFind":9},"id":28,"implicitStats":[{"stat":"armor","value":78}],"level":4,"name":"Cosmos Star","originalLevel":20,"power":2,"quality":3,"requires":[{"stat":"dex","value":7}],"slot":"legs","sprite":[6,2],"stats":{"addAttackCritMultiplier":19,"addSpellCritChance":48,"lvlRequire":16,"magicFind":13},"type":"Leather Pants","worth":249,"pos":36},{"enchantedStats":{"addAttackCritMultiplier":39,"dex":11,"regenMana":4},"id":29,"implicitStats":[{"stat":"addAttackCritMultiplier","value":16}],"level":20,"name":"Vagabond's Snare","power":3,"quality":4,"slot":"twoHanded","spell":{"castTimeMax":0,"cdMax":9,"name":"Melee","quality":1,"random":{"damage":[2.64,17.37]},"rolls":{"damage":0.2169786424448772},"statMult":1,"statType":"str","threatMult":4,"type":"melee","useWeaponRange":true,"values":{"damage":5.83}},"sprite":[9,3],"stats":{"addAttackCritMultiplier":39,"addCritMultiplier":20,"addSpellCritMultiplier":27,"dex":11,"elementArcaneResist":10,"elementFireResist":10,"int":8,"regenMana":4},"type":"Axe","worth":261,"pos":33},{"enchantedStats":null,"id":30,"implicitStats":[{"stat":"armor","value":62}],"level":15,"name":"Cruel Penance","quality":4,"requires":[{"stat":"str","value":18}],"slot":"feet","sprite":[7,0],"stats":{"addAttackCritChance":29,"addSpellCritMultiplier":12,"elementAllResist":5,"elementFrostResist":8,"sprintChance":11},"type":"Steel Boots","worth":308,"pos":28},{"id":31,"implicitStats":[{"stat":"armor","value":64}],"level":16,"name":"Kick Seeker","quality":4,"requires":[{"stat":"str","value":15}],"slot":"feet","sprite":[7,0],"stats":{"addCritChance":22,"addSpellCritMultiplier":16,"dex":5,"dodgeSpellChance":10,"manaMax":5},"type":"Steel Boots","worth":314,"pos":29},{"id":32,"material":true,"name":"Epic Essence","quality":3,"quantity":4,"sprite":[0,5],"pos":44},{"id":33,"name":"Tradesman's Pride","noSalvage":true,"quality":1,"quantity":4,"sprite":[0,0],"spritesheet":"server/mods/feature-cards/images/items.png","type":"Gambler's Card","pos":38},{"id":34,"material":true,"name":"Leather Scrap","quality":0,"quantity":23,"sprite":[0,7],"pos":47},{"id":35,"material":true,"name":"Cloth Scrap","quality":0,"quantity":19,"sprite":[0,1],"pos":48},{"id":36,"material":true,"name":"Magic Essence","quality":1,"quantity":1,"sprite":[0,3],"pos":45},{"id":37,"material":true,"name":"Iron Bar","quality":0,"quantity":13,"sprite":[0,0],"pos":46},{"id":38,"material":true,"name":"Common Essence","quality":0,"quantity":3,"sprite":[0,2],"pos":27},{"id":39,"material":true,"name":"Rare Essence","quality":2,"quantity":1,"sprite":[0,4],"pos":26},{"ability":true,"id":40,"level":15,"name":"Rune of Tranquility","quality":0,"spell":{"name":"Tranquility","rolls":{"regenPercentage":0.09958412984223625},"type":"aura","values":{"regenPercentage":4.59}},"sprite":[10,0],"worth":340,"pos":23},{"action":"reslot","description":"Rerolls an item's slot","id":41,"material":true,"name":"Dragon-Glass Idol","quality":3,"quantity":1,"sprite":[6,8],"pos":24},{"id":42,"implicitStats":[{"stat":"armor","value":89}],"level":13,"name":"Helmet","quality":1,"requires":[{"stat":"str","value":13}],"slot":"head","sprite":[0,0],"stats":{"dex":5,"regenMana":4},"type":"Helmet","worth":86,"pos":21},{"id":43,"name":"Godly Promise","noSalvage":true,"quality":1,"quantity":1,"sprite":[0,0],"spritesheet":"server/mods/feature-cards/images/items.png","type":"Gambler's Card","pos":22},{"id":44,"implicitStats":[{"stat":"regenMana","value":4}],"level":13,"name":"Ring","quality":1,"slot":"finger","sprite":[4,1],"stats":{"manaMax":4,"xpIncrease":4},"type":"Ring","worth":86,"pos":20},{"id":45,"implicitStats":[{"stat":"addAttackCritChance","value":11}],"level":13,"name":"Dagger","quality":1,"requires":[{"stat":"dex","value":13}],"slot":"oneHanded","spell":{"castTimeMax":0,"cdMax":3,"name":"Melee","quality":1,"random":{"damage":[0.88,5.79]},"rolls":{"damage":0.21495218579610442},"statMult":1,"statType":"dex","type":"melee","useWeaponRange":true,"values":{"damage":1.93}},"sprite":[9,2],"stats":{"manaMax":6,"vit":2},"type":"Dagger","worth":86,"pos":0},{"id":46,"implicitStats":[{"stat":"str","value":3}],"level":10,"name":"Pendant","originalLevel":13,"quality":1,"slot":"neck","sprite":[1,0],"stats":{"dex":2,"lvlRequire":3},"type":"Pendant","worth":86,"pos":1},{"id":47,"implicitStats":[{"stat":"dex","value":2}],"level":13,"name":"Noble Silence","quality":2,"slot":"neck","sprite":[1,2],"stats":{"castSpeed":5,"elementHolyPercent":2,"regenMana":3},"type":"Locket","worth":136,"pos":2},{"id":48,"implicitStats":[{"stat":"armor","value":65}],"level":13,"name":"Leather Cap","quality":1,"requires":[{"stat":"dex","value":9}],"slot":"head","sprite":[0,2],"stats":{"addCritChance":24,"addCritMultiplier":13},"type":"Leather Cap","worth":86,"pos":3},{"id":49,"implicitStats":[{"stat":"armor","value":20}],"level":11,"new":true,"name":"Leather Boots","quality":0,"requires":[{"stat":"dex","value":9}],"slot":"feet","sprite":[7,2],"stats":{"itemQuantity":25},"type":"Leather Boots","worth":46,"pos":4},{"id":50,"material":true,"name":"Skyblossom","quality":0,"quantity":2,"sprite":[1,2],"pos":5}]`;
let testData = JSON.parse(testDataRaw);

export const Default: Story<ItemImageProps> = Template.bind({});
Default.args = {
	item: testData[0],
};

export const NoBackground: Story<ItemImageProps> = Template.bind({});
NoBackground.args = {
	item: testData[0],
	noBackground: true,
};

export const NoBackgroundEmpty: Story<ItemImageProps> = Template.bind({});
NoBackgroundEmpty.args = {
	noBackground: true,
};

export const NoBackgroundEmptySlot: Story<ItemImageProps> = Template.bind({});
NoBackgroundEmptySlot.args = {
	noBackground: true,
	slot: 'head',
};

export const Selected: Story<ItemImageProps> = Template.bind({});
Selected.args = {
	item: testData[0],
	selected: true,
};

export const Empty: Story<ItemImageProps> = () => {
	return (
		<>
			{[
				null,
				'head',
				'neck',
				'chest',
				'hands',
				'finger-1',
				'waist',
				'legs',
				'feet',
				'trinket',
				'oneHanded',
				'offHand',
				'tool',
				'rune',
				'quick',
			].map((i, x) => {
				return <ItemImage key={x} slot={i as SlotType}></ItemImage>;
			})}
		</>
	);
};

export const Examples: Story<ItemImageProps> = () => {
	return (
		<>
			{testData.map((i, x) => {
				return (
					<ItemImage
						key={x}
						item={i}
						quantity={{
							showEq: true,
							showQs: true,
							showQuantity: true,
							showNew: true,
						}}
					></ItemImage>
				);
			})}
		</>
	);
};

export const CustomQuantity: Story<ItemImageProps> = () => {
	return (
		<>
			{testData.map((i, x) => {
				return (
					<ItemImage
						key={x}
						item={i}
						quantity={{
							quantityFunction: (item) => item.type ?? 'item',
						}}
					></ItemImage>
				);
			})}
		</>
	);
};

export const WithDropShadow: Story<ItemImageProps> = () => {
	return (
		<div style={{ backgroundColor: 'red' }}>
			{testData.map((i, x) => {
				return (
					<ItemImage
						key={x}
						item={i}
						noBackground
						dropShadow
					></ItemImage>
				);
			})}
		</div>
	);
};
