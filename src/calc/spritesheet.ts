import { IWDItem } from '../isleward';

/*
Updated Oct 15 2021
https://gitlab.com/Isleward/isleward/-/blob/deb6fbc9befafd5436c602ada17eff8a6abfc4be/src/client/ui/shared/renderItem.js

we ignore clientConfig.spriteSizes and item.spriteSize

should we implement this?
https://gitlab.com/Isleward/isleward/-/blob/deb6fbc9befafd5436c602ada17eff8a6abfc4be/src/client/css/main.less#L361
*/

export function getItemSpriteSize(item: IWDItem) {
	let size = 64;
	let margin = 0;

	if (item.type === 'skin') {
		size = 8;
		margin = 16;
	}

	return {
		size,
		margin,
	};
}

export function getItemSpritePath(item: IWDItem) {
	let spritesheetString = item.spritesheet || '/images/items.png';
	if (!item.spritesheet) {
		if (item.material) {
			spritesheetString = '/images/materials.png';
		} else if (item.quest) {
			spritesheetString = '/images/questItems.png';
		} else if (item.consumable || item.type === 'consumable') {
			// renderItem doesn't check the .consumable bool anymore?

			spritesheetString = '/images/consumables.png';
		} else if (item.skin || item.type === 'skin') {
			// renderItem doesn't check this flag anymore either

			spritesheetString = '/images/characters.png';
		}
	}
	return new URL(spritesheetString, 'https://play.isleward.com/').href;
}

export function getItemSpritePosition(item: IWDItem) {
	const { size } = getItemSpriteSize(item);

	const imgX = (-item.sprite[0] * size);
	const imgY = (-item.sprite[1] * size);
	const backgroundPosition = `${imgX}px ${imgY}px`;

	return backgroundPosition;
}

export function getItemSpriteBackground(item: IWDItem) {
	const spritesheet = getItemSpritePath(item);
	const backgroundPosition = getItemSpritePosition(item);
	const background = `url(${spritesheet}) no-repeat scroll ${backgroundPosition} / auto`;

	return background;
}

export function getItemSpriteStyle(item: IWDItem) {
	const { size, margin } = getItemSpriteSize(item);
	const background = getItemSpriteBackground(item);

	return {
		background,
		width: `${size}px`,
		height: `${size}px`,
		margin: `${margin}px`,
	};
}
