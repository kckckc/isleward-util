import { IWDItem } from '../isleward';

export type ItemQuantityFilters = {
	showEq?: boolean;
	showQs?: boolean;
	showNew?: boolean;
	showQuantity?: boolean;
	forceQuantity?: boolean;
	quantityFunction?: (item: IWDItem) => string;
};

export function getItemQuantity(
	item: IWDItem,
	options: ItemQuantityFilters = {}
) {
	const {
		showEq = false,
		showQs = false,
		showNew = false,
		showQuantity = true,
		forceQuantity = false,
		quantityFunction,
	} = options;

	let className = '';
	let quantity = '';

	/*
	Quantity Priority:
	- Quickslotted items
	- Equipped items
	- New, unequipped items
	- Items with quantity
		- ForceQuantity will show quantity = 1

	Quantity function will override the quantity text completely
	*/

	if (showQs && item.quickslot) {
		quantity += 'QS';
		className += 'eq';
	} else if (showEq && item.eq) {
		quantity += 'EQ';
		className += 'eq';
	} else if (showNew && item.new) {
		quantity += 'NEW';
		className += 'new';
	} else if (showQuantity || forceQuantity) {
		if (!forceQuantity) {
			// If we are using 'natural' quantity rules, show the quantity text when the item is >1
			if (item.quantity && item.quantity > 1) {
				quantity += item.quantity;
			}
		} else {
			// If forceQuantity is set, always show the quantity (assume 1 if no quantity is set)
			quantity += item.quantity ?? 1;
		}
	}

	if (quantityFunction) {
		quantity = quantityFunction(item);
		className = '';
	}

	let classNames = ['quantity', className].filter((a) => a.length > 0);

	return {
		quantity,
		classNames,
	};
}
