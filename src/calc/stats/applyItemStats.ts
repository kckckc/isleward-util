import { IWDItem } from '../../isleward';
import { Stats, StatType } from './stats';

export default function applyItemStats(
	objStats: Stats,
	{ stats, implicitStats }: IWDItem,
	isEq = true,
) {
	Object.keys(stats ?? {}).forEach((s: StatType) => {
		const value = stats[s];
		const useValue = isEq ? value : -value;
		objStats.addStat(s, useValue);
	});

	(implicitStats ?? []).forEach(({ stat, value }) => {
		const useValue = isEq ? value : -value;
		objStats.addStat(stat, useValue);
	});
}
