import { MAX_LEVEL, PASSIVE_TREE } from '../consts';
import { Stats, StatType } from './stats';

export function calcPoints(level, selected) {
	let points = level - selected.length + 1;

	if (level < MAX_LEVEL) {
		points -= 1;
	}

	return points;
}

export function applyPassiveNodes(stats: Stats, selected) {
	selected.forEach((id) => {
		const node = PASSIVE_TREE.nodes.find((n) => (n.id === id));

		if (node) {
			Object.keys(node.stats).forEach((p: StatType) => {
				stats.addStat(p, node.stats[p]);
			});
		}
	});
}
