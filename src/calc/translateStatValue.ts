const percentageStats = [
	'addCritChance',
	'addCritMultiplier',
	'addAttackCritChance',
	'addAttackCritMultiplier',
	'addSpellCritChance',
	'addSpellCritMultiplier',
	'sprintChance',
	'xpIncrease',
	'blockAttackChance',
	'blockSpellChance',
	'dodgeAttackChance',
	'dodgeSpellChance',
	'attackSpeed',
	'castSpeed',
	'itemQuantity',
	'magicFind',
	'catchChance',
	'catchSpeed',
	'fishRarity',
	'fishWeight',
	'fishItems',

	// Custom
	'critChance',
	'critMultiplier',
	'attackCritChance',
	'attackCritMultiplier',
	'spellCritChance',
	'spellCritMultiplier',
];

// TODO: how should we handle the types of statvalue?
//       it can be a string like '+123' too (from item comparison) and the + should be preserved
export default function translateStatValue(
	statName: string,
	statValue: any,
	noDiv: boolean = false,
): string {
	let res = statValue;

	res = Math.floor(res * 1000) / 1000;

	if (!noDiv && statName.indexOf('CritChance') > -1) {
		res = statValue / 20;
	}

	if (percentageStats.includes(statName) || statName.indexOf('Percent') > -1 || (statName.indexOf('element') === 0 && statName.indexOf('Resist') === -1)) {
		res += '%';
	}

	return res;
}
