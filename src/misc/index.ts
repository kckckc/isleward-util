import generateTooltip from './generateTooltip';
import generateText from './generateText';

export default {
	generateTooltip,
	generateText,
};
