# Isleward Util

Random React components + other utilities for Isleward addons.

Created from [HarveyD/react-component-library](https://github.com/HarveyD/react-component-library)

## Todo

* drag n drop
* make generatetooltip fall back to copying text if image fails
* check if genenratetooltip actually works (fix rollup errors?)
* item perfection, add to tooltips w/ a provider
* stat counting
* damage calculation
* fix linting
* figure out pipeline for updating our stuff when upstream iwd stuff changes (ex stattranslations, stringifystatvalue, spell/stat data, item tooltip lines, etc)

### Todo - Components

* itemtooltips near right side and bottom of screen should wrap/flip (contextmenus too)
* organize storybook better
* fix testing (dont use csf3 yet so we can still use storyshots and composestories)
* wait for storybook interactions
* @storybook/addon-a11y / axe

## Development

* `yarn install`
* `yarn generate MyComponent`
* `cd dist && yarn link`, `yarn dev` (rollup w/ watcher)
* `yarn test:watch`
* `yarn storybook`
