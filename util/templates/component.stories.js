module.exports = (componentName) => ({
	content: `// Generated with util/create-component.js
import React from 'react';
import { Meta, Story } from '@storybook/react';
import ${componentName} from './${componentName}';
import { ${componentName}Props } from './${componentName}.types';

export default {
	title: '${componentName}',
	component: ${componentName}
} as Meta;

const Template: Story<${componentName}Props> = (args) => <${componentName} {...args} />;

export const Default: Story<${componentName}Props> = Template.bind({});
Default.args = {
	children: 'foo'
}

export const OtherExample: Story<${componentName}Props> = Template.bind({});
OtherExample.args = {
	children: 'bar'
}
`,
	extension: `.stories.tsx`
});
