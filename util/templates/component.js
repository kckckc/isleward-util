module.exports = (componentName) => ({
	content: `// Generated with util/create-component.js
import React from 'react';

import { ${componentName}Props } from './${componentName}.types';

import styles from './${componentName}.module.scss';

const ${componentName}: React.FC<${componentName}Props> = ({ foo }) => {
	return (
		<div className={styles.${componentName}}>{foo}</div>
	);
};

export default ${componentName};
`,
	extension: `.tsx`,
});
