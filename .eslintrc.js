module.exports = {
	root: true,
	extends: [
		'eslint:recommended',
		'plugin:react/recommende ',
		'eslint-config-prettier',
		'plugin:storybook/recommended',
	],
	parserOptions: {
		project: './tsconfig.json',
	},
	rules: {
		// Disable props-spreading check because it's used in a HoC
		// 'react/jsx-props-no-spreading': 'off',
		// Disable prop-types check because TS does it already
		// 'react/prop-types': 'off',
	},
};
